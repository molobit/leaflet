const url = 'https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_week.geojson';
const urlAll = 'https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.geojson';

d3.json(urlAll).then((data) => { createFeatures(data.features); });

function getColor(val) {
    if (10 >= val && val >= -10)
        return '#8cf200';

    if (20 >= val && val > 10)
        return '#aaf300';

    if (30 >= val && val > 20)
        return '#c9f500';

    if (40 >= val && val > 30)
        return '#e8f600';

    if (50 >= val && val > 40)
        return '#f8e800';

    if (60 >= val && val > 50)
        return '#f9cc00';

    if (70 >= val && val > 60)
        return '#fbae00';

    if (80 >= val && val > 70)
        return '#fc9100';

    if (90 >= val && val > 80)
        return '#fe7300';

    if (val > 90)
        return '#ff5500';
}

function createFeatures(earthquakeData) {
    const onEachFeature = (feature, layer) => {
        layer.bindPopup("<h3>" + feature.properties.place +
          "</h3><hr><p>" + new Date(feature.properties.time) + "</p>");
    }

    const pointToLayer = (geoJsonPoint, latlng) => {
        const color = getColor(geoJsonPoint.geometry.coordinates[2]);
        return L.circleMarker(latlng, {
            radius: geoJsonPoint.properties.mag * 2,
            weight: 1,
            fill: true,
            color: 'black',
            fillColor: color,
            fillOpacity: 0.6
        });
    }

    const earthquakes = L.geoJSON(earthquakeData, {
        onEachFeature: onEachFeature,
        pointToLayer: pointToLayer
    });

    createMap(earthquakes);

    d3.select('.legend')
        .append('ul')
        .selectAll('ul')
        .data([
            {label: '10 / -10', color: getColor(10)},
            {label: '20 - 10',  color: getColor(20)},
            {label: '30 - 20',  color: getColor(30)},
            {label: '40 - 30',  color: getColor(40)},
            {label: '50 - 40',  color: getColor(50)},
            {label: '60 - 50',  color: getColor(60)},
            {label: '70 - 60',  color: getColor(70)},
            {label: '80 - 70',  color: getColor(80)},
            {label: '90 - 80',  color: getColor(90)},
            {label: '>90', color: getColor(100)},
        ])
        .enter()
        .append('li')
        .html(e => `${e.label} <span class="square" style="background-color: ${e.color}"></span>`);
}

function createMap(earthquakes) {
    const streetmap = L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
        attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
        tileSize: 512,
        maxZoom: 12,
        zoomOffset: -1,
        id: "mapbox/streets-v11",
        accessToken: API_KEY
    });

    const darkmap = L.tileLayer("https://api.mapbox.com/styles/v1/mapbox/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
        attribution: "Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"https://www.mapbox.com/\">Mapbox</a>",
        maxZoom: 12,
        id: "dark-v10",
        accessToken: API_KEY
    });

    const satellitemap = L.tileLayer("https://api.mapbox.com/styles/v1/mapbox/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
        attribution: "Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"https://www.mapbox.com/\">Mapbox</a>",
        maxZoom: 12,
        id: "satellite-v9",
        accessToken: API_KEY
    });

    const baseMaps = {
      "Satellite": satellitemap,
      "Dark Map": darkmap,
      "Street Map": streetmap
    };

    const overlayMaps = {
        Earthquakes: earthquakes
    };

    const map = L.map("mapid", {
        center: [40.73, -74.0059],
        zoom: 2,
        layers: [satellitemap, earthquakes]
    });

    L.control.layers(baseMaps, overlayMaps, {
        collapsed: false
    })
    .addTo(map);

    // Create a legend to display information about our map
    const info = L.control({
      position: "bottomright"
    });

    // When the layer control is added, insert a div with the class of "legend"
    info.onAdd = function() {
        let div = L.DomUtil.create("div", "legend");
        return div;
    };
    // Add the info legend to the map
    info.addTo(map);
}
